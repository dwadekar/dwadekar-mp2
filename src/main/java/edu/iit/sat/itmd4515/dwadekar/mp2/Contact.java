/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Contact_Info")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String emailAddress;
    private String mobileNumber;
    private String homeNumber;
    private String officeNumber;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfCreation;

    /**
     * Default constructor with no parameters for class Contact
     */
    public Contact() {
    }

    /**
     * Constructor with the following parameters passed to it
     * @param emailAddress
     * @param mobileNumber
     * @param homeNumber
     * @param officeNumber
     * @param dateOfCreation
     */
    public Contact(String emailAddress, String mobileNumber, String homeNumber, String officeNumber, Date dateOfCreation) {
        this.emailAddress = emailAddress;
        this.mobileNumber = mobileNumber;
        this.homeNumber = homeNumber;
        this.officeNumber = officeNumber;
        this.dateOfCreation = dateOfCreation;
    }

    /**
     * Returns the ID set for the given object
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID for the passed object
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the Email Address set for the given object
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Set the Email Address for the passed object
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Returns the Mobile Number set for the given object
     * @return
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Set the Mobile Number for the passed object
     * @param mobileNumber
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * Returns the Home Number set for the given object
     * @return
     */
    public String getHomeNumber() {
        return homeNumber;
    }

    /**
     * Set the Home Number for the passed object
     * @param homeNumber
     */
    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    /**
     * Return the Office Number set for the given object
     * @return
     */
    public String getOfficeNumber() {
        return officeNumber;
    }

    /**
     * Set the Office Number for the passed object
     * @param officeNumber
     */
    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    /**
     * Returns the date of creation set for the given object
     * @return
     */
    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    /**
     * Set the date of creation for the passed object
     * @param dateOfCreation
     */
    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @Override
    public String toString() {
        return String.format("%10s%10s%10s%11s%10s", getEmailAddress() + " |", getMobileNumber() + " |", getHomeNumber() + " |", getOfficeNumber() + " |", getDateOfCreation() + " |");
    }

}
