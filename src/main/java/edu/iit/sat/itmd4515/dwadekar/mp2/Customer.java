/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Customer_Info")
@NamedQueries({
    @NamedQuery(name = "Customer.findByName", query = "select c from Customer c where c.firstName = :name"),
    @NamedQuery(name = "Customer.findAll", query = "select c from Customer c")
})
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String relationShipType;
    private boolean isExistingCustomer;
    private Long branchId;
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "add_fk")
    private Address address;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_fk")
    private Contact contact;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "proof_fk")
    private List<ProofOfDocuments> documentProof;

    /**
     * Default constructor with no parameters for class Customer
     */
    public Customer() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param title
     * @param firstName
     * @param middleName
     * @param lastName
     * @param relationShipType
     * @param isExistingCustomer
     * @param branchId
     * @param dateOfBirth
     * @param creationDate
     */
    public Customer(String title, String firstName, String middleName, String lastName, String relationShipType, boolean isExistingCustomer, Long branchId, Date dateOfBirth, Date creationDate) {
        this.title = title;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.relationShipType = relationShipType;
        this.isExistingCustomer = isExistingCustomer;
        this.branchId = branchId;
        this.dateOfBirth = dateOfBirth;
        this.creationDate = creationDate;
    }

    /**
     * Returns the ID set for the given object
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID for the passed object
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the Title set for the given object
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the Title for the passed object
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the First Name set for the given object
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the First Name for the passed object
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the Middle Name set for the given object
     *
     * @return
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Set the Middle Name for the passed object
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Returns the Last Name set for the given object
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the Last Name for the passed object
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the Relationship type set for the given object
     *
     * @return
     */
    public String getRelationShipType() {
        return relationShipType;
    }

    /**
     * Set the Relationship Type for the passed object
     *
     * @param relationShipType
     */
    public void setRelationShipType(String relationShipType) {
        this.relationShipType = relationShipType;
    }

    /**
     * Returns whether Customer is existing or not set for the given object
     *
     * @return
     */
    public boolean isIsExistingCustomer() {
        return isExistingCustomer;
    }

    /**
     * Set whether Customer is existing or not for the passed object
     *
     * @param isExistingCustomer
     */
    public void setIsExistingCustomer(boolean isExistingCustomer) {
        this.isExistingCustomer = isExistingCustomer;
    }

    /**
     * Returns the Branch ID set for the given object
     *
     * @return
     */
    public Long getBranchId() {
        return branchId;
    }

    /**
     * Set the Branch ID for the passed object
     *
     * @param branchId
     */
    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    /**
     * Returns the Date of Birth set for the given object
     *
     * @return
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Set the Date of Birth for the passed object
     *
     * @param dateOfBirth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Returns the date of creation of customer set for the given object
     *
     * @return
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Set the date of creation of customer for the passed object
     *
     * @param creationDate
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Returns the Address of customer set for the given object
     *
     * @return
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Set the Address of customer for the passed object
     *
     * @param address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * Returns the Contact of customer set for the given object
     *
     * @return
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Set the Contact of customer for the passed object
     *
     * @param contact
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     * Returns the collection of address set for the given object
     *
     * @return
     */
    public List<ProofOfDocuments> getDocumentProof() {
        return documentProof;
    }

    /**
     * Set the collection of address for the passed object
     *
     * @param documentProof
     */
    public void setDocumentProof(List<ProofOfDocuments> documentProof) {
        this.documentProof = documentProof;
    }

    @Override
    public String toString() {
        //return "Customer{" + "id=" + id + ", title=" + title + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", relationShipType=" + relationShipType + ", isExistingCustomer=" + isExistingCustomer + ", branchId=" + branchId + ", dateOfBirth=" + dateOfBirth + ", creationDate=" + creationDate + '}';
        return String.format("%13s%35s%10s%10s%11s", getId() + " |", getTitle() + " " + getFirstName() + " " + getMiddleName() + " " + getLastName() + " |", getRelationShipType() + " |", getDateOfBirth() + " |", getBranchId() + " |");
    }

}
