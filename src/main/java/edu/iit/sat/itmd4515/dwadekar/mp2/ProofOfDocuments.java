/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Document_Proof")
public class ProofOfDocuments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String documentType;
    private String documentName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date submittedDate;
    private String status;

    /**
     * Default constructor with no parameters for class ProofOfDocuments
     */
    public ProofOfDocuments() {
    }

    /**
     * Constructor with the following parameters passed to it
     * @param documentType
     * @param documentName
     * @param submittedDate
     * @param status
     */
    public ProofOfDocuments(String documentType, String documentName, Date submittedDate, String status) {
        this.documentType = documentType;
        this.documentName = documentName;
        this.submittedDate = submittedDate;
        this.status = status;
    }

    /**
     * Returns the ID set for the given object
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID for the passed object
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the Document Type set for the given object
     * @return
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * Set the Document Type for the passed object
     * @param documentType
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    /**
     * Returns the Document Name set for the given object
     * @return
     */
    public String getDocumentName() {
        return documentName;
    }

    /**
     * Set the Document Name for the passed object
     * @param documentName
     */
    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    /**
     * Returns the Submitted Date of document set for the given object
     * @return
     */
    public Date getSubmittedDate() {
        return submittedDate;
    }

    /**
     * Set the Submitted Date of document for the passed object
     * @param submittedDate
     */
    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    /**
     * Return the Status of the document set for the given object
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set the Status of the document for the passed object
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("%20s%20s%20s%10s", getDocumentType() + " |", getDocumentName() + " |", getSubmittedDate() + " |", getStatus() + " |");
    }

}
