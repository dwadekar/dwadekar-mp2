/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author dwadekar
 */
@Entity
@Table(name = "RelationshipManager")
@NamedQueries({
    @NamedQuery(name = "RelationshipManager.findByName", query = "select r from RelationshipManager r where r.firtsName= :name"),
    @NamedQuery(name = "RelationshipManager.findAll", query = "select r from RelationshipManager r")
})
public class RelationshipManager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long branchID;
    private String title;
    private String firtsName;
    private String lastName;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_fk")
    private List<AssignedCustomers> customers = new ArrayList<>();

    /**
     * Default constructor with no parameters for class RelationshipManager
     */
    public RelationshipManager() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param branchID
     * @param title
     * @param firtsName
     * @param lastName
     */
    public RelationshipManager(Long branchID, String title, String firtsName, String lastName) {
        this.branchID = branchID;
        this.title = title;
        this.firtsName = firtsName;
        this.lastName = lastName;
    }

    /**
     * Returns the ID set for the given object
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID for the passed object
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Return the Branch ID set for the given object
     *
     * @return
     */
    public Long getBranchID() {
        return branchID;
    }

    /**
     * Set the Branch ID for the passed object
     *
     * @param branchID
     */
    public void setBranchID(Long branchID) {
        this.branchID = branchID;
    }

    /**
     * Return the Title set for the given object
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the Title for the passed object
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return the First Name set for the given object
     *
     * @return
     */
    public String getFirtsName() {
        return firtsName;
    }

    /**
     * Set the First Name for the passed object
     *
     * @param firtsName
     */
    public void setFirtsName(String firtsName) {
        this.firtsName = firtsName;
    }

    /**
     * Return the Last Name set for the given object
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the Last Name for the passed object
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the collection of Customers set for the given object
     *
     * @return
     */
    public List<AssignedCustomers> getCustomers() {
        return customers;
    }

    /**
     * Set the collection of Customers for the passed object
     *
     * @param customers
     */
    public void setCustomers(List<AssignedCustomers> customers) {
        this.customers = customers;
    }

    @Override
    public String toString() {
        return String.format("%15s%15s%20s", getId() + " |", getBranchID() + " |", getTitle() + " " + getFirtsName() + " " + getLastName() + " |");
    }

}
