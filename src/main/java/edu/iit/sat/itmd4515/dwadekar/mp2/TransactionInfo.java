/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Transaction_Info")
public class TransactionInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long customerID;
    private Long accountNo;
    private String typeOfTxn;
    private Float amount;
    private String modeOfTxn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date txnDate;

    /**
     * Default constructor with no parameters for class TransactionInfo
     */
    public TransactionInfo() {
    }

    /**
     * Constructor with the following parameters passed to it
     * @param customerID
     * @param accountNo
     * @param typeOfTxn
     * @param amount
     * @param modeOfTxn
     * @param txnDate
     */
    public TransactionInfo(Long customerID, Long accountNo, String typeOfTxn, Float amount, String modeOfTxn, Date txnDate) {
        this.customerID = customerID;
        this.accountNo = accountNo;
        this.typeOfTxn = typeOfTxn;
        this.amount = amount;
        this.modeOfTxn = modeOfTxn;
        this.txnDate = txnDate;
    }

    /**
     * Returns the ID set for the given object
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID for the passed object
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the Customer ID set for the given object
     * @return
     */
    public Long getCustomerID() {
        return customerID;
    }

    /**
     * Set the Customer ID for the passed object
     * @param customerID
     */
    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    /**
     * Returns the Account No set for the given object
     * @return
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Set the Account No for the passed object
     * @param accountNo
     */
    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * Returns the Type Of transaction set for the given object
     * @return
     */
    public String getTypeOfTxn() {
        return typeOfTxn;
    }

    /**
     * Set the Type Of transaction for the passed object
     * @param typeOfTxn
     */
    public void setTypeOfTxn(String typeOfTxn) {
        this.typeOfTxn = typeOfTxn;
    }

    /**
     * Returns the Amount of transaction set for the given object 
     * @return
     */
    public Float getAmount() {
        return amount;
    }

    /**
     * Set the Amount of transaction for the passed object
     * @param amount
     */
    public void setAmount(Float amount) {
        this.amount = amount;
    }

    /**
     * Returns the mode of transaction set for the given object
     * @return
     */
    public String getModeOfTxn() {
        return modeOfTxn;
    }

    /**
     * Set the mode of transaction for the passed object
     * @param modeOfTxn
     */
    public void setModeOfTxn(String modeOfTxn) {
        this.modeOfTxn = modeOfTxn;
    }

    /**
     * Returns the transaction date set for the given object
     * @return
     */
    public Date getTxnDate() {
        return txnDate;
    }

    /**
     * Set the transaction date set for the given object
     * @param txnDate
     */
    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }

    @Override
    public String toString() {
        return String.format("%14s%22s%15s%10s%20s%30s", getCustomerID() + " |", getAccountNo() + " |", getTypeOfTxn() + " |", getAmount() + " |", getModeOfTxn() + " |", getTxnDate() + " |");
    }

}
