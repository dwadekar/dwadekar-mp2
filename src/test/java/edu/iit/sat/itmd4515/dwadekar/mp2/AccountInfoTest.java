/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dwadekar
 */
public class AccountInfoTest {
    
    private static EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction tx;

    /**
     * Default constructor with no parameters for class AccountInfoTest
     */
    public AccountInfoTest() {
    }

    /**
     * Before Class test method initializes Entity Manager factory
     */
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("dwadekarPU");
        System.out.println("------------------------------------------------------------");
        System.out.println("#################### AccountInfoTest ####################");
        System.out.println("------------------------------------------------------------");
    }

    /**
     * After Class test method closes the Entity Manager factory
     */
    @AfterClass
    public static void tearDownClass() {
        emf.close();
    }

    /**
     * Before test method creates the Entity Manager before each test case
     */
    @Before
    public void setUp() {
        em = emf.createEntityManager();
        tx = em.getTransaction();
        
        long randomNum = Math.abs(new Random().nextLong());
        AccountInfo account1 = new AccountInfo(randomNum, 1L, "Savings Account", "", 500.23F, "USD", new Date());
        TransactionInfo txnInfo11 = new TransactionInfo(1L, randomNum, "D", 150F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo21 = new TransactionInfo(1L, randomNum, "C", 25F, "RTGS Transfer", new Date());
        List<TransactionInfo> transactions = new ArrayList<>();
        transactions.add(txnInfo11);
        transactions.add(txnInfo21);
        account1.setTransactions(transactions);
        
        long randomNum2 = Math.abs(new Random().nextLong());
        AccountInfo account2 = new AccountInfo(randomNum, 2L, "Current Account", "", 1235.23F, "USD", new Date());
        TransactionInfo txnInfo12 = new TransactionInfo(2L, randomNum, "D", 600F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo22 = new TransactionInfo(2L, randomNum, "C", 500F, "RTGS Transfer", new Date());
        List<TransactionInfo> transactions2 = new ArrayList<>();
        transactions2.add(txnInfo12);
        transactions2.add(txnInfo22);
        account2.setTransactions(transactions2);
        
        tx.begin();
        em.persist(account1);
        em.persist(txnInfo11);
        em.persist(txnInfo21);
        em.flush();
        em.persist(account2);
        em.persist(txnInfo12);
        em.persist(txnInfo22);
        tx.commit();
    }

    /**
     * After closes the Entity manager after each test method and deletes all
     * record from table
     */
    @After
    public void tearDown() {
        List<AccountInfo> accounts = em.createQuery("select a from AccountInfo a", AccountInfo.class).getResultList();
        for (AccountInfo a : accounts) {
            tx.begin();
            em.remove(a);
            tx.commit();
            List<TransactionInfo> transactions = a.getTransactions();
            for (TransactionInfo txn : transactions) {
                tx.begin();
                em.remove(txn);
                tx.commit();
            }
        }
        em.close();
    }

    /**
     * Test Method to test the initialization of Persistence in project
     */
    @Test
    public void testInitPU() {
        assertTrue(true);
    }

    /**
     * Test method to test creation of new account and transactions intialised
     * in account
     */
    @Test
    public void testPersistNewAccount() {
        long randomNum = Math.abs(new Random().nextLong());
        AccountInfo account = new AccountInfo(randomNum, 3L, "Savings Account", "", 1000.23F, "USD", new Date());
        TransactionInfo txnInfo1 = new TransactionInfo(3L, randomNum, "C", 100F, "ATM Withdrawal", new Date());
        TransactionInfo txnInfo2 = new TransactionInfo(3L, randomNum, "C", 50F, "RTGS Transfer", new Date());
        List<TransactionInfo> transactions = new ArrayList<>();
        transactions.add(txnInfo1);
        transactions.add(txnInfo2);
        account.setTransactions(transactions);
        
        tx.begin();
        em.persist(account);
        em.persist(txnInfo1);
        em.persist(txnInfo2);
        em.flush();
        tx.commit();
        
        assertNotNull(account.getId());
    }

    /**
     * Test method to find account and transaction information
     */
    @Test
    public void testFindCustomerAccountInfo() {
        AccountInfo accInfo = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 1L).getSingleResult();
        assertEquals(accInfo.getCustomerID(), Long.valueOf(1L));
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT INFORMATION (TEST METHOD : testFindCustomerAccountInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%22s%14s%20s%22s%20s%10s%30s", "Account No" + " |", "Customer ID" + " |", "Account Type" + " |", "Account Description" + " |", "Available Balance" + " |", "Currency" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(accInfo);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER'S TRANSACTION INFORMATION (TEST METHOD : testFindCustomerAccountInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%14s%22s%15s%10s%20s%30s", "Customer ID" + " |", "Account No" + " |", "Type Of Txn" + " |", "Amount" + " |", "Mode of Txn" + " |", "Date of Txn" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<TransactionInfo> txnInfo = accInfo.getTransactions();
        for (Iterator it = txnInfo.iterator(); it.hasNext();) {
            Object o = it.next();
            if (o instanceof TransactionInfo) {
                System.out.println(o);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
    }

    /**
     * Test method to update the account information
     */
    @Test
    public void testUpdateAccountInfo() {
        AccountInfo accInfo = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 1L).getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT BEFORE UPDATION INFORMATION (TEST METHOD : testUpdateAccountInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%22s%14s%20s%22s%20s%10s%30s", "Account No" + " |", "Customer ID" + " |", "Account Type" + " |", "Account Description" + " |", "Available Balance" + " |", "Currency" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(accInfo);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        
        tx.begin();
        accInfo.setAvailableBalance(600.23F);
        accInfo.setCurrency("EUR");
        tx.commit();
        
        AccountInfo accInfo2 = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 1L).getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT AFTER UPDATION INFORMATION (TEST METHOD : testUpdateAccountInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%22s%14s%20s%22s%20s%10s%30s", "Account No" + " |", "Customer ID" + " |", "Account Type" + " |", "Account Description" + " |", "Available Balance" + " |", "Currency" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(accInfo2);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        
        assertEquals(accInfo2.getAvailableBalance(), Float.valueOf(600.23F));
        assertEquals(accInfo2.getCurrency(), "EUR");
    }

    /**
     * Test method to update the Transactions information
     */
    @Test
    public void testUpdateTransactionInfo() {
        AccountInfo accInfo = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S TRANSACTION BEFORE UPDATION INFORMATION (TEST METHOD : testUpdateTransactionInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%14s%22s%15s%10s%20s%30s", "Customer ID" + " |", "Account No" + " |", "Type Of Txn" + " |", "Amount" + " |", "Mode of Txn" + " |", "Date of Txn" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<TransactionInfo> txnInfo = accInfo.getTransactions();
        for (Iterator it = txnInfo.iterator(); it.hasNext();) {
            Object o = it.next();
            if (o instanceof TransactionInfo) {
                System.out.println(o);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
        
        tx.begin();
        for (TransactionInfo txn : txnInfo) {
            if (txn.getTypeOfTxn().equals("C")) {
                txn.setModeOfTxn("NEFT Transfer");
            }
        }
        tx.commit();
        
        AccountInfo accInfo2 = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S TRANSACTION AFTER UPDATION INFORMATION (TEST METHOD : testUpdateTransactionInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%14s%22s%15s%10s%20s%30s", "Customer ID" + " |", "Account No" + " |", "Type Of Txn" + " |", "Amount" + " |", "Mode of Txn" + " |", "Date of Txn" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<TransactionInfo> txnInfo2 = accInfo2.getTransactions();
        for (Iterator it = txnInfo2.iterator(); it.hasNext();) {
            Object o = it.next();
            if (o instanceof TransactionInfo) {
                System.out.println(o);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
        
        for (TransactionInfo txn : txnInfo2) {
            if (txn.getModeOfTxn().equals("NEFT Transfer")) {
                assertTrue("Record Updated Successfully", true);
            }
        }
    }

    /**
     * Test method to delete account information
     */
    @Test
    public void testDeleteAccountInfo() {
        AccountInfo accInfo = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getSingleResult();
        List<TransactionInfo> transactions = accInfo.getTransactions();
        
        tx.begin();
        em.remove(accInfo);
        tx.commit();
        
        for (TransactionInfo txn : transactions) {
            tx.begin();
            em.remove(txn);
            tx.commit();
        }
        List<AccountInfo> accInfo2 = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getResultList();
        if (accInfo2.isEmpty()) {
            assertTrue("Record deleted sucessfully!!", true);
        }
    }

    /**
     * Test method to update new balance of customer account
     */
    @Test
    public void testNewBalanceInCustomersAccount() {
        AccountInfo accInfo = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getSingleResult();
        List<TransactionInfo> transactions = accInfo.getTransactions();
        
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT BEFORE UPDATION INFORMATION (TEST METHOD : testNewBalanceInCustomersAccount) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%22s%14s%20s%22s%20s%10s%30s", "Account No" + " |", "Customer ID" + " |", "Account Type" + " |", "Account Description" + " |", "Available Balance" + " |", "Currency" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(accInfo);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER'S TRANSACTION INFORMATION (TEST METHOD : testNewBalanceInCustomersAccount) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%14s%22s%15s%10s%20s%30s", "Customer ID" + " |", "Account No" + " |", "Type Of Txn" + " |", "Amount" + " |", "Mode of Txn" + " |", "Date of Txn" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        for (Iterator it = transactions.iterator(); it.hasNext();) {
            Object o = it.next();
            if (o instanceof TransactionInfo) {
                System.out.println(o);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
        
        for (TransactionInfo txn : transactions) {
            if (txn.getTypeOfTxn() == "C") {
                float newBalance;
                newBalance = accInfo.getAvailableBalance() + txn.getAmount();
                tx.begin();
                accInfo.setAvailableBalance(newBalance);
                tx.commit();
            }
            if (txn.getTypeOfTxn() == "D") {
                float newBalance;
                newBalance = accInfo.getAvailableBalance() - txn.getAmount();
                tx.begin();
                accInfo.setAvailableBalance(newBalance);
                tx.commit();
            }
        }
        
        AccountInfo accInfo2 = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", 2L).getSingleResult();
        List<TransactionInfo> transactions2 = accInfo.getTransactions();
        
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT AFTER UPDATION INFORMATION (TEST METHOD : testNewBalanceInCustomersAccount) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%22s%14s%20s%22s%20s%10s%30s", "Account No" + " |", "Customer ID" + " |", "Account Type" + " |", "Account Description" + " |", "Available Balance" + " |", "Currency" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(accInfo2);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER'S TRANSACTION INFORMATION (TEST METHOD : testNewBalanceInCustomersAccount) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%14s%22s%15s%10s%20s%30s", "Customer ID" + " |", "Account No" + " |", "Type Of Txn" + " |", "Amount" + " |", "Mode of Txn" + " |", "Date of Txn" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        for (Iterator it = transactions2.iterator(); it.hasNext();) {
            Object o = it.next();
            if (o instanceof TransactionInfo) {
                System.out.println(o);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
    }
    
    /**
     * Test method to find all Account Information
     */
    @Test
    public void testFindAllAccountInfo() {
        List<AccountInfo> accsInfo = em.createNamedQuery("AccountInfo.findAll", AccountInfo.class).getResultList();
        assertTrue(accsInfo.size() > 0);
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER'S ACCOUNT INFORMATION (TEST METHOD : testFindAllAccountInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        for (AccountInfo acc : accsInfo) {
            System.out.println(acc);
            for (TransactionInfo txn : acc.getTransactions()) {
                System.out.println(txn);
            }
            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        System.out.println("*****************************************************************************************************************************************************");
    }
}
