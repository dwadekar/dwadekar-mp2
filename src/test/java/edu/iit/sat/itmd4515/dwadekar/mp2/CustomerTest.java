/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dwadekar
 */
public class CustomerTest {
    
    private static EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction tx;

    /**
     * Default constructor with no parameters for class CustomerTest
     */
    public CustomerTest() {
    }

    /**
     * Before Class test method initializes Entity Manager factory
     */
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("dwadekarPU");
        System.out.println("------------------------------------------------------------");
        System.out.println("#################### CustomerTest ####################");
        System.out.println("------------------------------------------------------------");
    }

    /**
     * After Class test method closes the Entity Manager factory
     */
    @AfterClass
    public static void tearDownClass() {
        emf.close();
    }

    /**
     * Before test method creates the Entity Manager before each test case
     */
    @Before
    public void setUp() {
        em = emf.createEntityManager();
        tx = em.getTransaction();
        
        Customer cust = new Customer("Ms.", "Remanda", "D.", "Samuel", "Personal Account", false, 00154L, new GregorianCalendar(1985, 8, 14).getTime(), new Date());
        Address address1 = new Address("12", "Arlington House", 5, "Arthur Road", "Chicago", "Illinois", "United States", 615203, new GregorianCalendar(1985, 1, 15).getTime());
        Contact contact1 = new Contact("xyz2@gmail.com", "1-800-123458", "256488979", "35679568", new Date());
        ProofOfDocuments documentProof1 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof2 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList1 = new ArrayList<>();
        documentProofList1.add(documentProof1);
        documentProofList1.add(documentProof2);
        cust.setDocumentProof(documentProofList1);
        
        Customer cust2 = new Customer("Mrs.", "Akriti", "G.", "Tandon", "Personal Account", false, 00357L, new GregorianCalendar(1978, 4, 8).getTime(), new Date());
        Address address2 = new Address("20/7", "Bombay House", 6, "Washigton Road", "Washington", "New York", "United States", 614283, new GregorianCalendar(1993, 4, 10).getTime());
        Contact contact2 = new Contact("abc132@gmail.com", "1-320-1212458", "23545688", "32149568", new Date());
        ProofOfDocuments documentProof3 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof4 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList2 = new ArrayList<>();
        documentProofList2.add(documentProof3);
        documentProofList2.add(documentProof4);
        cust2.setDocumentProof(documentProofList2);
        
        Customer cust3 = new Customer("Mr.", "Alaister", "K.", "Cook", "Personal Account", false, 00564L, new GregorianCalendar(1992, 7, 4).getTime(), new Date());
        Address address3 = new Address("2/1", "George House", 10, "Wall Road", "Dadar", "Mumbai", "India", 415878, new GregorianCalendar(1995, 10, 25).getTime());
        Contact contact3 = new Contact("lmn5@yahoo.com", "1-800-954858", "256488979", "341312568", new Date());
        ProofOfDocuments documentProof5 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof6 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        List<ProofOfDocuments> documentProofList3 = new ArrayList<>();
        documentProofList3.add(documentProof5);
        documentProofList3.add(documentProof6);
        cust3.setDocumentProof(documentProofList3);
        
        cust.setAddress(address1);
        cust2.setAddress(address2);
        cust3.setAddress(address3);
        cust.setContact(contact1);
        cust2.setContact(contact2);
        cust3.setContact(contact3);
        tx.begin();
        em.persist(cust);
        em.persist(address1);
        em.persist(contact1);
        em.persist(documentProof1);
        em.persist(documentProof2);
        em.flush();
        em.persist(cust2);
        em.persist(address2);
        em.persist(contact2);
        em.persist(documentProof3);
        em.persist(documentProof4);
        em.flush();
        em.persist(cust3);
        em.persist(address3);
        em.persist(contact3);
        em.persist(documentProof5);
        em.persist(documentProof6);
        tx.commit();
    }

    /**
     * After closes the Entity manager after each test method and deletes all
     * record from table
     */
    @After
    public void tearDown() {
        List<Customer> customers = em.createQuery("select c from Customer c", Customer.class).getResultList();
        for (Customer c : customers) {
            tx.begin();
            em.remove(c);
            tx.commit();
            Contact contact = c.getContact();
            tx.begin();
            em.remove(contact);
            tx.commit();
            Address add = c.getAddress();
            tx.begin();
            em.remove(add);
            tx.commit();
            for (ProofOfDocuments docs : c.getDocumentProof()) {
                tx.begin();
                em.remove(docs);
                tx.commit();
            }
        }
        em.close();
    }

    /**
     * Test Method to test the initialization of Persistence in project
     */
    @Test
    public void testInitPU() {
        assertTrue(true);
    }

    /**
     * Test method to create the customer information
     */
    @Test
    public void testPersistNewCustomer() {
        Customer cust = new Customer("Mr.", "Robert", "N.", "Williams", "Personal Account", false, 00254L, new GregorianCalendar(2015, 10, 21).getTime(), new Date());
        Address address = new Address("7/1", "Santa House", 16, "Arthur Road", "Koolaba", "Mumbai", "India", 415003, new GregorianCalendar(2001, 9, 2).getTime());
        Contact contact = new Contact("robert.williams@yahoo.com", "1-845-554858", "275869979", "381224568", new Date());
        ProofOfDocuments documentProof7 = new ProofOfDocuments("Address Proof", "Passport", new Date(), "submitted");
        ProofOfDocuments documentProof8 = new ProofOfDocuments("ID Proof", "Driving License", new Date(), "submitted");
        cust.setAddress(address);
        cust.setContact(contact);
        List<ProofOfDocuments> documentProofList4 = new ArrayList<>();
        documentProofList4.add(documentProof7);
        documentProofList4.add(documentProof8);
        cust.setDocumentProof(documentProofList4);
        
        tx.begin();
        em.persist(cust);
        em.persist(address);
        em.persist(contact);
        em.persist(documentProof7);
        em.persist(documentProof8);
        tx.commit();
        
        assertNotNull(cust.getId());
    }

    /**
     * Test method to find customer information
     */
    @Test
    public void testFindCustomer() {
        Customer cust = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", "Remanda").getSingleResult();
        assertEquals(cust.getFirstName(), "Remanda");
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER INFORMATION (TEST METHOD : testFindCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%10s%35s%18s%30s%5s", "Customer ID" + " |", "Customer Name" + " |", "Account Type" + " |", "Date of Birth" + " |", "Branch ID" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(cust);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER ADDRESS INFORMATION (TEST METHOD : testFindCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%10s%15s%10s%17s%15s%10s%10s%10s%30s", "House No" + " |", "House Name" + " |", "Building No" + " |", "Street Name" + " |", "City Name" + " |", "Town Name" + " |", "Country Name" + " |", "Postal Code" + " |", "Present Address Date" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        Address add = cust.getAddress();
        System.out.println(add);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER CONTACT INFORMATION (TEST METHOD : testFindCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%27s%14s%11s%10s%30s", "Email Address" + " |", "Mobile No" + " |", "Home No" + " |", "Office No" + " |", "Date of Creation" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        Contact cont = cust.getContact();
        System.out.println(cont);
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("######### CUSTOMER'S DOCUMENT PROOF INFORMATION (TEST METHOD : testFindCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%20s%20s%30s%11s", "Document Type" + " |", "Document Name" + " |", "Date of Submission" + " |", "Status" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<ProofOfDocuments> docs = cust.getDocumentProof();
        for (Iterator it = docs.iterator(); it.hasNext();) {
            Object b = it.next();
            if (b instanceof ProofOfDocuments) {
                System.out.println(b);
            }
        }
        System.out.println("*****************************************************************************************************************************************************");
    }

    /**
     * Test method to update the customer information
     */
    @Test
    public void testUpdateCustomer() {
        Customer cust = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", "Akriti").getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER INFORMATION BEFORE UPDATION (TEST METHOD : testUpdateCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%10s%35s%18s%30s%5s", "Customer ID" + " |", "Customer Name" + " |", "Account Type" + " |", "Date of Birth" + " |", "Branch ID" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(cust);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        
        tx.begin();
        cust.setDateOfBirth(new GregorianCalendar(1991, 3, 11).getTime());
        cust.setLastName("Smith");
        tx.commit();
        
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER INFORMATION AFTER UPDATION (TEST METHOD : testUpdateCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%10s%35s%18s%30s%5s", "Customer ID" + " |", "Customer Name" + " |", "Account Type" + " |", "Date of Birth" + " |", "Branch ID" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(cust);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        
        Customer cust2 = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", "Akriti").getSingleResult();
        assertEquals(cust2.getDateOfBirth(), new GregorianCalendar(1991, 3, 11).getTime());
        assertEquals(cust2.getLastName(), "Smith");
    }

    /**
     * Test method to delete the customer information
     */
    @Test
    public void testDeleteCustomer() {
        Customer cust = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", "Alaister").getSingleResult();
        Address add = cust.getAddress();
        Contact contact = cust.getContact();
        tx.begin();
        em.remove(cust);
        em.remove(add);
        em.remove(contact);
        tx.commit();
        for (ProofOfDocuments docs : cust.getDocumentProof()) {
            tx.begin();
            em.remove(docs);
            tx.commit();
        }
        List<Customer> custs = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", "Alaister").getResultList();
        if (custs.isEmpty()) {
            assertTrue("Record Deleted successfully", true);
        }
    }
    
    /**
     * Test method to find all Customers
     */
    @Test
    public void testFindAllCustomers() {
        List<Customer> customers = em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
        assertTrue(customers.size() > 0);
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### CUSTOMER INFORMATION (TEST METHOD : testFindAllCustomer) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        for (Customer cust : customers) {
            System.out.println(cust);
            Address add = cust.getAddress();
            System.out.println(add);
            Contact contact = cust.getContact();
            System.out.println(contact);
            for (ProofOfDocuments docs : cust.getDocumentProof()) {
                System.out.println(docs);
            }
            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        System.out.println("*****************************************************************************************************************************************************");
    }
}
