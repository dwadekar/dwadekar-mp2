/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.mp2;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dwadekar
 */
public class RelationshipManagerTest {
    
    private static EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction tx;

    /**
     * Default constructor with no parameters for class RelationshipManagerTest
     */
    public RelationshipManagerTest() {
    }

    /**
     * Before Class test method initializes Entity Manager factory
     */
    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory("dwadekarPU");
        System.out.println("------------------------------------------------------------");
        System.out.println("#################### RelationshipManagerTest ####################");
        System.out.println("------------------------------------------------------------");
    }

    /**
     * After Class test method closes the Entity Manager factory
     */
    @AfterClass
    public static void tearDownClass() {
        emf.close();
    }

    /**
     * Before test method creates the Entity Manager before each test case
     */
    @Before
    public void setUp() {
        em = emf.createEntityManager();
        tx = em.getTransaction();
        
        RelationshipManager rmanager = new RelationshipManager(561L, "Mr.", "Sung", "Li");
        AssignedCustomers customer1 = new AssignedCustomers(1L, "Mr. Quinton de Kock");
        AssignedCustomers customer2 = new AssignedCustomers(2L, "Ms. R Samuel");
        List<AssignedCustomers> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        rmanager.setCustomers(customers);
        
        RelationshipManager rmanager2 = new RelationshipManager(125L, "Ms.", "Fatena", "Goshtz");
        AssignedCustomers customer12 = new AssignedCustomers(3L, "Mr. James Pattison");
        List<AssignedCustomers> customers2 = new ArrayList<>();
        customers2.add(customer12);
        rmanager2.setCustomers(customers2);
        
        RelationshipManager rmanager3 = new RelationshipManager(26L, "Mr.", "Michael", "Bevan");
        AssignedCustomers customer13 = new AssignedCustomers(4L, "Ms. Amrita Goswami");
        AssignedCustomers customer23 = new AssignedCustomers(5L, "Ms. Akriti Tandon");
        AssignedCustomers customer33 = new AssignedCustomers(6L, "Mr. Ian Bell");
        List<AssignedCustomers> customers3 = new ArrayList<>();
        customers3.add(customer13);
        customers3.add(customer23);
        customers3.add(customer33);
        rmanager3.setCustomers(customers3);
        
        tx.begin();
        em.persist(rmanager);
        em.persist(customer1);
        em.persist(customer2);
        em.flush();
        em.persist(rmanager2);
        em.persist(customer12);
        em.flush();
        em.persist(rmanager3);
        em.persist(customer13);
        em.persist(customer23);
        em.persist(customer33);
        tx.commit();
    }

    /**
     * After closes the Entity manager after each test method and deletes all
     * record from table
     */
    @After
    public void tearDown() {
        List<RelationshipManager> rmanagers = em.createQuery("select r from RelationshipManager r", RelationshipManager.class).getResultList();
        for (RelationshipManager m : rmanagers) {
            tx.begin();
            em.remove(m);
            tx.commit();
            List<AssignedCustomers> customers = m.getCustomers();
            for (AssignedCustomers cust : customers) {
                tx.begin();
                em.remove(cust);
                tx.commit();
            }
        }
        em.close();
    }

    /**
     * Test Method to test the initialization of Persistence in project
     */
    @Test
    public void testInitPU() {
        assertTrue(true);
    }

    /**
     * Test method to create the new relationship manager and customer's
     * assigned to him
     */
    @Test
    public void testPersistNewRelationshipManager() {
        RelationshipManager rmanager = new RelationshipManager(231L, "Mr.", "Eyck", "Hou");
        AssignedCustomers customer1 = new AssignedCustomers(1L, "Mr. Alaister Cook");
        AssignedCustomers customer2 = new AssignedCustomers(2L, "Ms. Remanda Samuel");
        List<AssignedCustomers> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        rmanager.setCustomers(customers);
        
        tx.begin();
        em.persist(rmanager);
        em.persist(customer1);
        em.persist(customer2);
        tx.commit();
        
        assertNotNull(rmanager.getId());
    }

    /**
     * Test method to find relationship manager and it's assigned customer's
     * list
     */
    @Test
    public void testFindRelationshipManager() {
        RelationshipManager rmanager = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Michael").getSingleResult();
        assertEquals(rmanager.getFirtsName(), "Michael");
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### RELATIONSHIP MANAGER INFORMATION (TEST METHOD : testFindRelationshipManager) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%15s%20s", "Manager ID" + " |", "Branch ID" + " |", "Manager Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(rmanager);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("The following customers are assign to " + rmanager.getTitle() + " " + rmanager.getFirtsName() + " " + rmanager.getLastName());
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%20s", "Customer ID" + " |", "Customer Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<AssignedCustomers> customers = rmanager.getCustomers();
        for (AssignedCustomers cust : customers) {
            System.out.println(cust);
        }
        System.out.println("*****************************************************************************************************************************************************");
    }

    /**
     * Test method to update the relationship manager information
     */
    @Test
    public void testUpdateRelationshipManagerInfo() {
        RelationshipManager rmanager = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### RELATIONSHIP MANAGER BEFORE UPDATION INFORMATION (TEST METHOD : testUpdateRelationshipManagerInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%15s%20s", "Manager ID" + " |", "Branch ID" + " |", "Manager Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(rmanager);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        
        tx.begin();
        rmanager.setLastName("Tunney");
        rmanager.setBranchID(321L);
        tx.commit();
        
        RelationshipManager rmanager2 = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### RELATIONSHIP MANAGER AFTER UPDATION INFORMATION (TEST METHOD : testUpdateRelationshipManagerInfo) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%15s%20s", "Manager ID" + " |", "Branch ID" + " |", "Manager Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println(rmanager2);
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        assertEquals(rmanager2.getLastName(), "Tunney");
        assertEquals(rmanager2.getBranchID(), Long.valueOf(321));
    }

    /**
     * Test method to update the assigned customers list
     */
    @Test
    public void testUpdateAssignedCustomers() {
        RelationshipManager rmanager = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("The following customers are assign to " + rmanager.getTitle() + " " + rmanager.getFirtsName() + " " + rmanager.getLastName() + " (BEFORE UPDATION (TEST METHOD : testUpdateRelationshipManagerInfo))");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%20s", "Customer ID" + " |", "Customer Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<AssignedCustomers> customers = rmanager.getCustomers();
        for (AssignedCustomers cust : customers) {
            System.out.println(cust);
        }
        System.out.println("*****************************************************************************************************************************************************");
        
        tx.begin();
        for (AssignedCustomers cust : customers) {
            if (cust.getCustomerName().equals("Mr. James Pattison")) {
                cust.setCustomerName("Mr. James Purcell");
            }
        }
        tx.commit();
        
        RelationshipManager rmanager2 = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getSingleResult();
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("The following customers are assign to " + rmanager2.getTitle() + " " + rmanager2.getFirtsName() + " " + rmanager2.getLastName() + " (AFTER UPDATION (TEST METHOD : testUpdateRelationshipManagerInfo))");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.format("%15s%20s", "Customer ID" + " |", "Customer Name" + " |");
        System.out.println("\n-----------------------------------------------------------------------------------------------------------------------------------------------------");
        List<AssignedCustomers> customers2 = rmanager2.getCustomers();
        for (AssignedCustomers cust2 : customers2) {
            System.out.println(cust2);
        }
        System.out.println("*****************************************************************************************************************************************************");
        for (AssignedCustomers cust2 : customers2) {
            if (cust2.getCustomerName().equals("Mr. James Purcell")) {
                assertEquals(cust2.getCustomerName(), "Mr. James Purcell");
            }
        }
    }

    /**
     * Test method to delete relationship manager information
     */
    @Test
    public void testDeleteRelationshipManager() {
        RelationshipManager rmanager = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getSingleResult();
        tx.begin();
        em.remove(rmanager);
        tx.commit();
        List<AssignedCustomers> customers = rmanager.getCustomers();
        for (AssignedCustomers cust : customers) {
            tx.begin();
            em.remove(cust);
            tx.commit();
        }
        
        List<RelationshipManager> rmanager2 = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", "Fatena").getResultList();
        if (rmanager2.isEmpty()) {
            assertTrue("Record deleted successfully", true);
        }
    }
    
    /**
     * Test method to find all Relationship Managers
     */
    @Test
    public void testFindAllRelationshipManagers() {
        List<RelationshipManager> rmanagers = em.createNamedQuery("RelationshipManager.findAll", RelationshipManager.class).getResultList();
        assertTrue(rmanagers.size() > 0);
        System.out.println("*****************************************************************************************************************************************************");
        System.out.println("######### RELATIONSHIP MANAGER INFORMATION (TEST METHOD : testFindAllRelationshipManagers) ##########");
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        for (RelationshipManager manager : rmanagers) {
            System.out.println(manager);
            for (AssignedCustomers customer : manager.getCustomers()) {
                System.out.println(customer);
            }
            System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        System.out.println("*****************************************************************************************************************************************************");
    }
}
